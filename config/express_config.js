var express = require('express'),
    morgan = require('morgan'),
    compress = require('compression'),
    bodyParser = require('body-parser'),
    methodOverride = require('method-override'),
    config = require('./config'),              
    session = require('express-session'),
    passport = require('passport'),    //추가
    flash = require('connect-flash')
    ;


module.exports = function() {
    var app = express();
 
    if(process.env.NODE_ENV === 'development') {
        app.use(morgan('dev'));
    } else if (process.env.NODE_ENV === 'production') {
        app.use(compress());
    }
 
    app.use(bodyParser.urlencoded({
        extended : true
    }));
    app.use(bodyParser.json());
    app.use(methodOverride());

    app.use(session({                        // New!! start
        saveUninitialized : true,
        resave : true,
        secret : config.sessionSecret
    }));    

    app.set('views', './app/views');           // New!!
    app.set('view engine', 'ejs');             // New!!

    app.use(flash());
    app.use(passport.initialize());    // 추가
    app.use(passport.session());       // 추가

    require('../app/routes/index.server.routes.js')(app);
    require('../app/routes/users.server.routes.js')(app);  // 라우팅 추가

    app.use(express.static('./static')); 

    return app;
}


/*
var express = require('express'); 
module.exports = function() { 
	var app = express(); 
	require('../app/routes/index.server.routes.js')(app); 
	return app; 
}
*/