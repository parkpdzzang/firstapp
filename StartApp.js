process.env.NODE_ENV = process.env.NODE_ENV || 'development';
 
var express = require('./config/express_config'),
	mongoose = require('./config/mongoose'),
	passport = require('./config/passport'); 


var db = mongoose();
var app = express();
var passport = passport();

app.listen(3000);
module.exports = app;
 
console.log('Server running at localhost');


  

/*
var express = require('./config/express_config'); 
var app = express(); 
app.listen(3000); 
module.exports = app; 
console.log('Server running at localhost');
*/

/*
var http = require('http');
http.createServer(function(req,res) {
	res.writeHead(200, 
		{ 'Content-Type' : 'text/plain' 
	});
	res.end('Hello World'); 
}).listen(3000);
 console.log('Server running at http://localhost:3000/');
*/
/*
var express = require('express'); 
var app = express(); 

app.use('/', function(req,res){
	res.send('Hello World');
});

app.listen(3000); 
console.log('Server running at http://localhost:3000'); 
*/

/*
var express = require('express');
var app = express(); 
var question = function(req, res, next) {
	if(req.param('answer')) {
		next();
	}
	else { 
		res.send('so what is your answer?')
	}
}; 

var result = function(req,res) { 
	res.send('good') 
} 

app.get('/', question , result);
app.listen(3000); 
console.log('server start');
*/